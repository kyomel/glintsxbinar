const express = require('express');
const morgan = require('morgan');

const exception = require('./middlewares/errorHandler');
const router = require('./router');

require('dotenv').config();

const app = express();
const { PORT = 3000 } = process.env;

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
})

app.use(express.json());
app.use(morgan('dev'));
app.use('/api/v1', router);


exception.forEach(handler =>
  app.use(handler)
);

app.listen(PORT, () => {
  console.log(`Server started at ${Date()}`);
  console.log(`Listening on port ${PORT}`);
})
