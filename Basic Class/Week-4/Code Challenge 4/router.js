const router = require("express").Router();

const postController = require("./controllers/postController");
const userController = require("./controllers/userController");
const responsesHandler = require("./middlewares/responseHeader");
const authenticate = require("./middlewares/authenticate");
const authorize = require("./middlewares/authorize");
const checkAdmin = require("./middlewares/checkAdmin");

/* Post Api Collections */
router.get("/posts", checkAdmin, postController.getAll, responsesHandler);
router.post("/posts", authenticate, postController.create, responsesHandler);
router.delete("/posts/:id", authenticate, checkAdmin, authorize('Post'), postController.delete, responsesHandler);
router.patch("/approve/posts/:id", authenticate, checkAdmin, postController.approve, responsesHandler);
router.patch("/posts/:id", authenticate, authorize('Post'), postController.update, responsesHandler);

/* User Api Collections */
router.post("/users/register", userController.register, responsesHandler);
router.post("/users/login", userController.login, responsesHandler);
router.get("/me", authenticate, responsesHandler);

module.exports = router;
