const models = require("../models");

module.exports = (modelname) => {
  let target = models[modelname];
  return async (req, res, next) => {
    if (res.admin) {
      next();
    }
    let item = await target.findByPk(req.params.id);
    if (item.user_id !== res.user.id) {
      res.status(403);
      next(new Error("You are not allowed to do this"));
    } else {
      next();
    }
  };
};
