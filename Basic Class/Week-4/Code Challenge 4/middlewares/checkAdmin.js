const { User } = require("../models");
const { verify } = require("jsonwebtoken");

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let instance = verify(token, process.env.SECRET_KEY);
    let findUser = await User.findByPk(instance.id);
    if (findUser.role == 'admin') {
      res.admin = true;
    }
    next();
  } catch (error) {
    next();
  }
}
