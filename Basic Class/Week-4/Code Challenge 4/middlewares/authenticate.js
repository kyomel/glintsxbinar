const { User }=require('../models');
const { verify }=require('jsonwebtoken');

module.exports = async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    res.status(401);
    next(new Error("Token can not be empty!"));
  }
  try {
    let instance = verify(token, process.env.SECRET_KEY);
    let findUser = await User.findByPk(instance.id);
    res.user = findUser.entity;
    next();
  } catch (error) {
    res.status(401);
    next(error);
  }
};
