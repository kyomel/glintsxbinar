'use strict';

const { hash, genSaltSync, compareSync} = require ('bcryptjs');
const { signToken } = require ('jsonwebtoken');
require ('dotenv').config();

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      allownull: false,
      unique: true,
      validate:{
        notEmpty:true,
      }
    },
    password: {
      type: DataTypes.STRING,
      allownull: false,
    },
    name: {
      type:DataTypes.STRING,
      allownull: false,
    },
    role: {
      type:DataTypes.STRING,
      allownull: false,
    }
  }, {
    tableName:'Users'
  });

  Object.defineProperty(User.prototype, 'entity' , {
    get(){
      return{
        id:this.id,
        username:this.username,
        access_token:this.getToken(),
      };
    },
  });

  User.associate = function(models) {
    User.hasMany(models.Post, {
      foreignkey:"user_id",
    });
  
    User.beforeCreate(async (user, options) => {
      const hashedPassword = await hash(user.password, genSaltSync(10));
      user.password = hashedPassword;
    });
  
    User.prototype.getToken = function () {
      return signToken({ id: this.id, username: this.username }, process.env.SECRET_KEY);
    };
  
    User.prototype.checkCredentials = function (password) {
      return compareSync(password, this.password);
    };
  
  };
  return User;
};