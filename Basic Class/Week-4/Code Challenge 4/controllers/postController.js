const { User, Post }=require ('../models');

module.exports = {
    getAll: async (req, res, next) => {
      res.data = await Post.findAll({ include: ["author"] });
      if (!res.admin) {
        res.data = res.data.filter((a) => a.approved == true);
      }
      return next();
    },
    
    create: async (req, res, next) => {
      try {
        let post = await Post.create({
          title: req.body.title,
          body: req.body.body,
          user_id: res.user.id,
        });
        res.data = post;
        next();
      } catch (error) {
        res.status(400);
        next(error);
      }
    },
    
    delete: async (req, res, next) => {
      await Post.destroy({ where: { id: req.params.id } });
      res.status(203);
      res.end();
    },
    
    update: async (req, res, next) => {
      try {
        if (req.body.approved !== null) {
          delete req.body.approved;
        }
        await Post.update({ ...req.body }, { where: { id: req.params.id } });
        res.status(200);
        res.data = await Post.findByPk(req.params.id);
        next();
      } catch (error) {
        res.status(400);
        next(error);
      }
    },
    
    approve: async (req, res, next) => {
      if (res.admin) {
        await Post.update({ approved: true }, { where: { id: req.params.id } });
        res.data = await Post.findByPk(req.params.id);
        next();
      } else {
        res.status(403);
        next(new Error("You can not approve!"));
      }
    },
  };
  