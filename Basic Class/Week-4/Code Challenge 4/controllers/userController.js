const { User } = require("../models");

module.exports = {
  register: async (req, res, next) => {
    try {
      let newUser = await User.findOrCreate({
        where: { username: req.body.username },
        defaults: {
          password: req.body.password,
        },
      });

      if (!newUser[1]) {
        res.status(400);
        next(new Error("Username has already been taken!"));
      }
      res.data = newUser[0];
      res.status(201);
      next();
    } catch (error) {
      next(error.message);
    }
  },
  login: async (req, res, next) => {
    try {
      let user = await User.findOne({ where: { username: req.body.username } });
      if (!user) {
        res.status(400).json({ status: "fail", message: "No such username" });
      }
      let result = user.checkCredentials(req.body.password);
      if (!result) {
        res.status(400);
        next(new Error("password is not correct!"));
      }
      res.data = user.entity;
      next();
    } catch (error) {
      next(error.message);
    }
  },
};
