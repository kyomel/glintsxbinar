const {Post}= require('../models');
const {User}=require('../models');

//Using async to find user_id at post and compares that with User id so if match they can edit and delete
module.exports = async (req,res,next) => {
    try {
        let check = await Post.findByPk(req.params.id);
        if(check.user_id == req.User.id)
        {
            next();
        }
        else{
            res.status(403).json({
                status:'fail',
                message:'You can not do this!'
            });
        }
    } catch (error) {
        res.status(401);
        next(error);
    }
}