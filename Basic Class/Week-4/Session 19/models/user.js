'use strict';
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        isLowercase: true,
        notEmpty: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  },  {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance ? instance.email.toLowerCase() : "";
      },
      beforeCreate: instance => {
        instance.encrypted_password = bcrypt.hashSync(instance.encrypted_password, 10);
      },
    },
    tableName:'Users'
  });

  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Post, {
      foreignKey: 'user_id'
    })
  };

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        verified: this.verified,
        access_token: this.getToken() 
      }
    }
  });

  User.authenticate = async function({ email, password }) {
    try {
      let instance = await this.findOne({
        where: { email: email.toLowerCase() }
      })
      if (!instance) return Promise.reject(new Error("Email doesn't exist"));
      
      let isValidPassword = instance.checkCredential(password);
      if (!isValidPassword) return Promise.reject(new Error("Wrong password!"));
    
      return Promise.resolve(instance);
    }

    catch(err) {
      return Promise.reject(err);
    }
  }

  User.prototype.checkCredential = function(password) {
    /* It will return true or false
      Depending on the result of bcrypt.compareSync */
    return bcrypt.compareSync(password, this.encrypted_password);
  }

  User.prototype.getToken = function() {
    return jwt.sign({
      id: this.id,
      email: this.email
    }, process.env.SECRET_KEY);
  }

  return User;
};