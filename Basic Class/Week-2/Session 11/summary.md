<h2>Event Emitter and Object Oriented Programming</h2>
<p>All objects that emit events are members of EventEmitter class. These objects expose an eventEmitter.on() function that allows one or more functions to be attached to named events emitted by the object.</p></br>
<p>Object Oriented Programming (OOP) is a programming paradigm based on the concept of "objects", which can contain data, in the form of fields (often known as attributes or properties), and code, in the form of procedures (often known as methods). A feature of objects is an object's procedures that can access and often modify the data fields of the object with which they are associated (objects have a notion of "this" or "self"). In OOP, computer programs are designed by making them out of objects that interact with one another. OOP languages are diverse, but the most popular ones are class-based, meaning that objects are instances of classes, which also determine their types.</p></br>
<p>The concept is quite simple: emitter objects emit named events that cause previously registered listeners to be called. So, an emitter object basically has two main features:</p></br>
<ul>
<li>Emitting name events</li>
<li>Registering and unregistering listener functions</li>
</ul></br>
<p>Javascript have 3 types OOP:</p></br>
<ul>
<li>Factory Function</li>
<li>Constructor Function</li>
<li>Class Declaration</li>
</ul></br>
<p>In class Javascript they have 2 method and property</p></br>
<ul>
<li>Static method and property</li>
<li>Instance method and property</li>
</ul></br>
<p>Every Class have visibility:</p></br>
<ul>
<li>Public</li>
<li>Private</li>
<li>Protected</li>
</ul>