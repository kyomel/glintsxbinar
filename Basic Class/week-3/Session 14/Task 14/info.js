module.exports = {
    info(req, res) {
        res.status(200).json({
            "status": true,
            "data": [
                "POST /calculate/circleArea",
                "POST /calculate/squareArea",
                "POST /calculate/cubeVolume",
                "POST /calculate/triangleArea",
                "POST /calculate/tubeVolume"
            ]   
        })
    }
}