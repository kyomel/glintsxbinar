const express = require('express');
const router = express.Router();

const {
    info
} = require('./info.js')
const {
    circleArea
} = require('./controllers/circle.js');
const {
    squareArea
} = require('./controllers/square.js');
const {
    cubeVolume
} = require('./controllers/cube.js');
const {
    triangleArea
} = require('./controllers/triangle.js');
const {
    tubeVolume
} = require('./controllers/tube.js');


router.get('/', info);
router.post('/calculate/circleArea', circleArea);
router.post('/calculate/squareArea', squareArea);
router.post('/calculate/cubeVolume', cubeVolume);
router.post('/calculate/triangleArea', triangleArea);
router.post('/calculate/tubeVolume', tubeVolume);

module.exports = router;