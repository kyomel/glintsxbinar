const circle = require('../db/circle.json');
module.exports = {
    circleArea(req, res){
        let result = 2 * req.body.phi * req.body.r;
        return res.status(200).json({
            "Name": 'Circle',
            "Phi": req.body.phi,
            "R": req.body.r,
            "Area": result
        })
    }
}