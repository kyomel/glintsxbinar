const tube = require('../db/tube.json');
module.exports = {
    tubeVolume(req, res){
        let result = req.body.phi * req.body.r * req.body.r * req.body.h;
        return res.status(200).json({
            "Name": 'Tube',
            "Phi": req.body.phi,
            "R": req.body.r,
            "Height": req.body.h,
            "Volume": result
        })
    }
}