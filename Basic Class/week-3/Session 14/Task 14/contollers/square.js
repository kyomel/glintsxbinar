const square = require('../db/square.json');
module.exports = {
    squareArea(req, res){
        let result = Math.pow(req.body.s,2);
        return res.status(200).json({
            "Name": 'Square',
            "Side": req.body.s,
            "Area": result
        })
    }
}
