const triangle = require('../db/triangle.json');
module.exports = {
    triangleArea(req, res){
        let result = 0.5 * req.body.h * req.body.b;
        return res.status(200).json({
            "Name": 'Triangle',
            "Height": req.body.h,
            "Base": req.body.b,
            "Area": result
        })
    }
}