const express = require('express');
const router = express.Router();
const product = require('./controllers/productController');
const user = require('./controllers/userController');

/* =====Product API Collection===== */
router.get('/products', product.all);
router.get('/product', product.show);
router.post('/products', product.create);
router.put('/products/:id', product.update);
router.get('/products/available', product.available);
router.delete('/products/:id', product.delete);

/* =====User API Collection===== */
router.post('/users/register', user.register)
router.post('/users/login',user.login)
router.get('/users', user.getUser)
router.get('/users/me', user.me)
router.put('/users/:id', user.update)
router.delete('/users/:id', user.delete)


module.exports = router;
