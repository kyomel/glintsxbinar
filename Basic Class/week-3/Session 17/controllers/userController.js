const { User } = require('../models');
const authorizer = require(`./authorizer`)

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = {
  
  register:(req,res) => {
    req.body.email = req.body.email || ""
    let getEmail = (req.body.email).toLowerCase()
    console.log(`${getEmail} : ${req.body.password}`)
    User.create({
      email: getEmail,
      encrypted_password: req.body.password
    })
      .then(user => {
        res.status(201).json({
          status: `success`,
          data: {
            user: user.entity
          }
        })
      })
      .catch(err => {
        res.status(422).json({
          status: `fail`,
          errors: [err.message]
        })
      })
  },

  getUser: (req, res) => {
    authorizer.check()
    authorizer.checkAuthorization(req.headers.authorization)
    .then( () => {
      User.findAll().then(data => {
        res.status(200).json({
          status: "success",
          data
        })
      })
    }).catch(err => {
      res.status(422).json({
        status: `fail`,
        errors: [err.message]
      })
    })
  },

  login: (req, res) => {
    User.authenticate(req.body)
      .then(data => {
        res.status(201).json({
          status: `success`,
          data: data.entity
        })
      })
      .catch(err => {
        res.status(401).json({
          status: `fail`,
          errors: [err.message]
        })
      })
  },
  
  me(req, res) {
    authorizer.checkAuthorization(req.headers.authorization)
    .then( data => {
      User.findByPk(data.id)
      .then(user => {
      res.status(201).json({
        status: `success`,
        data: {
          user: user.entity
        }
      })
    })
    })
  },

  update(req, res) {
    authorizer.checkAuthorization(req.headers.authorization)
    .then( () => {
      User.update(
        req.body, {
          where: {
            id: req.params.id
          }
      }).then(data => {
        res.status(200).json({
          message: `data from id ${req.params.id} is updated`
        })
      })
    })
  },

  delete(req, res){
    authorizer.checkAuthorization(req.headers.authorization)
    .then( () => {
      User.destroy({
        where: {
          id: req.params.id
        }
      }).then(data => {
        res.status(204).json({
          message: `data from id ${req.params.id} is deleted`
        })
      })
    }).catch(err => {
      res.status(404).json({
        message: `${err.message}`
      })
    })
  }
}
