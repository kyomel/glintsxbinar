const jwt = require(`jsonwebtoken`)

async function checkAuthorization (token) {
    let payload

    try {
      if(!token) return Promise.reject(new Error(`Invalid Token`))
      payload = jwt.verify(token, process.env.TOKEN)
      return Promise.resolve(payload)
    } catch (error) {
      return Promise.reject(new Error(`Invalid Token`))
    }
  }

  function test() {
      console.log(`nah`)
  }

  module.exports = {
    checkAuthorization,test
  }
