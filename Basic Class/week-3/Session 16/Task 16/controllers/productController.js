const { Product } = require('../models');
const { Op } = require('sequelize');

module.exports = {
    all(req,res){
        Product.findAll().then(data => {
            res.status(200).json({
                status: 'true',
                data: {
                    products: data
                }
            })
        })
    },

    //Using Query Params
    show(req,res){
        Product.findOne({
            where: {
                id: req.query.id
            }
        })
        .then(product =>{
            res.status(200).json({
                status: 'Success',
                data :{
                    product
                }
            })
        })
    },

    create(req,res){
        Product.create(req.body)
        .then(product =>{
            res.status(201).json({
                status: 'Success',
                data:{
                    product:data
                }
            });
        })
        .catch(err =>{
            res.status(422).json({
                status: 'Fail',
                errors: [err.message]
            });
        })
    },

    //Using Path params
    update(req,res){
        Product.update(req.body,{
            where:{
                id:req.params.id
            }
        })
        .then(data =>{
            res.status(200).json({
                status:'Success',
                message:`Product with ID {reg.params.id} is successfully updated!`
            })
        })
        .catch(err =>{
            res.status(422).json({
                status:'Fail',
                errors: [err.message]
            })
        })
    },

    available(req,res){
        Product.findAll({
            where:{
                stock:{
                    [Op.gt]:0
                }
            }
        })
        .then(products =>{
            res.status(200).json({
                status:'Success',
                data:{
                    products
                }
            })
        })
    },

    delete(req,res){
        Product.destroy({
            where:{
                id: req.params.id
            }
        })
        .then(() => {
            res.status(204).json({
                status:'Success',
                message: `Product with ID{req.params.id} is successfully deleted!`
            })
        })
        .catch(err =>{
            res.status(422).json({
                status:'Fail',
                errors: [err.message]
            })
        })
    }
}