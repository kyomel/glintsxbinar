const express = require(`express`)
const router = express.Router()
const product = require(`./controllers/productController`)
const user = require(`./controllers/userController`)

router.get(`/products`,product.all)
router.get(`/products/available`,product.available)
router.get(`/products/:find`,product.find)
router.post(`/products/insert`, product.insert)
router.put(`/products/:id`, product.update)
router.delete(`/products/:id`, product.delete)

router.post(`/users/register`, user.register)
router.post(`/users/login`,user.login)
router.get(`/users`, user.getUser)

module.exports = router
