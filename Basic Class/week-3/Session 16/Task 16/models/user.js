'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        notNull: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      default: true
    }
  }, {
    underscored: true,
  });
  User.associate = function(models) {
    // associations can be defined here
  };

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        verified: this.verified
      }
    }
  })
  
  return User;
};
