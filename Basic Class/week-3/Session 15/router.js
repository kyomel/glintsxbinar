const express = require(`express`)
const router = express.Router()
const product = require(`./controllers/productController`)

router.get(`/products`,product.all)
router.get(`/products/available`,product.available)
router.post(`/products/insert`, product.insert)
router.post(`/products/update`, product.update)
router.post(`/products/delete`, product.delete)

module.exports = router
