const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {

  async register(req, res) {
    req.body.email = req.body.email || "";

    try {
      let user = await User.create({
        email: req.body.email.toLowerCase(),
        encrypted_password: req.body.password
      })

      res.status(201).json({
        status: 'success',
        data: {
          user
        }
      })
    }

    catch(err) {
      next(err);
    }
  },

  login(req, res) {
    User.authenticate(req.body)
      .then(data => {
        res.status(201).json({
          status: 'success',
          data: data.entity
        })
      })
      .catch(err => {
        res.status(401);
        next(err);
      })
  },

  me(req, res) {    
    res.status(200).json({
      status: 'success',
      data: {
        user: req.user
      }
    })
  }

}
