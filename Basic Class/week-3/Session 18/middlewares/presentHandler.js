module.exports = (req, res) => {
    res.status(res.statusCode).json({
        data : req.data
    })
}
