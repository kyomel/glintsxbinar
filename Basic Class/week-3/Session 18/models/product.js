'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER
  }, {
    underscored: false,
    tableName: 'Products'
  });
  Product.associate = function(models) {
    // associations can be defined here
  };
  return Product;
};
