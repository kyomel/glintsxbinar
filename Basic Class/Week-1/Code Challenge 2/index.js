const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  // We call clean function in order to filter our array
  data = clean(data);
  
  for (let i = 0; i < data.length; i++){
    // We need to iterate the array
    // And check each element against the next element in array
    for ( let j = data.length ; j >= i; j--){
        // if the current element is larger than the next element, swap them
        if(data[j] > data[j+1]) {
            let temp = data[j];
            data[j] = data[j+1];
            data[j+1] = temp;
        }
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  // We call clean function in order to filter our array
  data = clean(data);
  for (let i = 0; i < data.length; i++){
    // We need to iterate the array
    // And check each element against the next element in array
    for ( let j = data.length ; j >= i; j--){
        // if the next element is larger than the current element, swap them
        if(data[j] < data[j+1]) {
            let temp = data[j];
            data[j] = data[j+1];
            data[j+1] = temp;
        }
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
